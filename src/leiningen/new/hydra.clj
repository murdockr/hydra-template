(ns leiningen.new.hydra
  (:require [leiningen.new.templates :refer [renderer name-to-path ->files]]
            [leiningen.core.main :as main]))

(def render (renderer "hydra"))

(defn hydra
  [name]
  (let [data {:name name
              :sanitized (name-to-path name)}]
    (main/info "Generating fresh 'lein new' hydra project.")
    (->files data
             ["src/clj/{{sanitized}}/server.clj" (render "server.clj" data)]
             ["src/clj/{{sanitized}}/render.clj" (render "render.clj" data)]
             ["src/cljs/{{sanitized}}/client.cljs" (render "client.cljs" data)]
             ["src/clj/{{sanitized}}/routing.clj" (render "routing.clj" data)]
             ["src/cljs/{{sanitized}}/routing.cljs" (render "routing.cljs" data)]
             ["src/dev/figwheel.cljs" (render "figwheel.cljs" data)]
             ["src/components/components/components.cljs" (render "components.cljs" data)]
             ["README.md" (render "README.md" data)]
             ["project.clj" (render "project.clj" data)])))
