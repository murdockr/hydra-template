(ns {{name}}.routing
  (:require [goog.events :as events]
            [goog.History.EventType :as EventType]
            [cljs.core.async :refer [<! >! chan put!]]
            [secretary.core :as sec :refer-macros [defroute]]
            [cljs.reader :refer [read-string]]
            [ajax.core :refer [GET]])
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:import goog.history.Html5History
           goog.Uri))

(defn init-history [app-state]

  (defroute "/" []
    (swap! app-state assoc :view :home))

  (let [history (Html5History.)
        listen (fn [el type]
                 (let [out (chan)]
                   (events/listen el type
                                  (fn [e] (put! out e)))
                   out))
        navigation (listen history EventType/NAVIGATE)]
    (doto history
      (.setUseFragment false)
      (.setPathPrefix "")
      (.setEnabled true)) 
    (go
      (while true
        (let [token (.-token (<! navigation))]
          (sec/dispatch! token))))
    (events/listen js/document "click"
                   (fn [e]
                     (let [path (.getPath (.parse Uri (.-href (.-target e))))
                           title (.-title (.-target e))]
                       (when (sec/locate-route path) 
                         (. history (setToken path title))
                         (.preventDefault e)))))))
