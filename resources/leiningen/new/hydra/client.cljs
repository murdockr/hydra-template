(ns {{name}}.client
  (:require [om.core :as om :include-macros true]
            [components.components :refer [main]]
            [{{name}}.routing :refer [init-history]]
            [cljs.reader :as edn]
            [goog.dom :as gdom]))

(enable-console-print!)

(declare app-state)

(defn render [app-id]
  (om/root main
           app-state
           {:target app-id}))

(defn ^:export init
  [app-id state-id]
  (if (nil? app-state)
    (->> state-id
         gdom/getElement
         .-textContent
         edn/read-string
         atom
         (set! app-state)))
  (->> app-id
       gdom/getElement
       (render))
  (init-history app-state))
