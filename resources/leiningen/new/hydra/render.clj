(ns {{name}}.render
  (:require
    [hiccup.page :refer [html5 include-css include-js]]
    [clojure.java.io :as io])
  (:import [javax.script Invocable ScriptEngineManager]))

(defn minify-css
  [css]
  (-> css
      (clojure.string/replace #"[\n|\r]" "")
      (clojure.string/replace #"/\*.*?\*/" "")
      (clojure.string/replace #"\s+" " ")
      (clojure.string/replace #"\s*:\s*" ":")
      (clojure.string/replace #"\s*,\s*" ",")
      (clojure.string/replace #"\s*\{\s*" "{")
      (clojure.string/replace #"\s*}\s*" "}")
      (clojure.string/replace #"\s*;\s*" ";")
      (clojure.string/replace #";}" "}")))

(def js (.getEngineByName (ScriptEngineManager.) "nashorn"))

(doto js
  (.eval "var global = this")
  (.eval (-> "public/server/components.js"
             io/resource
             io/reader)))

(defn render-to-string
  [edn]
  (.invokeMethod
    ^Invocable js
    (.eval js "components.components")
    "render_to_string"
    (-> edn
        pr-str
        list
        object-array)))

(def render-components (memoize render-to-string))

(defn dev-wrapper
  [edn]
  (html5 
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible"
             :content "IE-edge"}]
     [:meta {:name "description" :content "changeme"}]
     [:meta {:name "mobile-web-app-capable" :content "yes"}]
     [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:title "{{name}}"]
     ]
    [:body
     [:div#app
      (render-components edn)]
     [:script#state {:type "application/edn"} edn]
     (include-js "/out/goog/base.js")
     (include-js "/js/main.js")
     [:script {:type "text/javascript"} "goog.require ('{{name}}.client')"]
     [:script {:type "text/javascript"} "goog.require ('figwheel')"]
     [:script "{{name}}.client.init('app', 'state')"]]))

(defn write-resource
  [resource]
  (-> resource
      slurp
      str))

(defn write-css
  [css pre]
  (concat pre
          (-> css
              write-resource
              minify-css)))

(def resource-memo (memoize write-resource))
(def css-memo (memoize write-css))

(defn render-wrapper
  [edn]
  (html5 
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:http-equiv "X-UA-Compatible"
             :content "IE-edge"}]
     [:meta {:name "description" :content "{{name}} - Isomorphic webapps in clojure"}]
     [:meta {:name "mobile-web-app-capable" :content "yes"}]
     [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:title "{{name}}"]]
    [:body
     [:div#app
      (render-components edn)]
     [:script#state {:type "application/edn"} edn]
     [:script {:type "text/javascript"} 
      (resource-memo "resources/public/js/main.js")]
     [:script "{{name}}.client.init('app', 'state')"]]))

(def render (memoize render-wrapper))
