(ns {{name}}.server
  (:require [org.httpkit.server :refer [run-server]]
            [{{name}}.routing :refer [routes]]
            [taoensso.timbre :as timbre])
  (:use [ring.middleware.edn]
        [ring.middleware.resource]
        [ring.middleware.file-info])
  (:gen-class))
(timbre/refer-timbre)

(defn key->caps
  [k]
  (-> k name clojure.string/upper-case))

(defn logging 
  [handler]
  (fn [req]
    (let [{remote-addr :remote-addr 
           headers :headers 
           request-method :request-method
           content-length :content-length
           uri :uri} req
          {user-agent "user-agent"} headers]
      (info remote-addr (key->caps request-method) uri user-agent content-length))
    (handler req)))



(defn edn-response [data & [status]]
  {:status (or status 200)
   :headers {"Content-Type" "application/edn"}
   :body (pr-str data)})


(defn -main [& {:as args}]
  (run-server (-> routes
                  (logging)
                  (wrap-resource "public")
                  (wrap-file-info))
              {:host "0.0.0.0"
               :port 8080}))
