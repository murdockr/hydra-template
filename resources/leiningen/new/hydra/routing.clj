(ns {{name}}.routing
       (:require [{{name}}.render :refer [dev-wrapper]]
                 [clojure.edn :as edn]
                 [compojure.core :refer [GET defroutes]]
                 [compojure.route :refer [resources not-found]]))

(def not-nil?
  (comp not nil?))

(defn edn-response [data & [status]]
        {:status (or status 200)
         :headers {"Content-Type" "application/edn"}
         :body (pr-str data)})

(defroutes routes
  (GET "/" [] (dev-wrapper {:view :home}))
  (resources "/")
  (not-found "<h1>Page Not Found"))
