(ns components.components
  (:require [om.core :as om :include-macros true]
            [om.dom :refer [render-to-str]]
            [om-tools.core :refer-macros [defcomponent defcomponentmethod]]
            [om-tools.dom :as dom :include-macros true]
            [cljs.reader :as edn]))

;; Fix console print on Nashorn and browser
(if (exists? js/console)
  (enable-console-print!)
  (set-print-fn! js/print))

(defmulti body
  (fn [data owner]
    (:view data)))

(defcomponentmethod body :home
  [data owner]
  (render [_]
    (dom/p "This is just an empty page. Get to work and add some content!")))

(defcomponent main
  [data owner]
  (render [_]
    (om/build body data)))

(defn ^:export render-to-string
  "Server-Side Render Helper"
  [edn]
  (->> edn
       edn/read-string
       (om/build main)
       render-to-str))
