(defproject {{name}} "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src/clj"]
  :clean-targets ^{:protect false} ["resources/public/out" "resources/public/server" "resources/public/js/main.js"]
  :main {{name}}.server
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-3119"]
                 [hiccup "1.0.5"]
                 [http-kit "2.1.16"]
                 [fogus/ring-edn "0.2.0"]
                 [compojure "1.3.2"]
                 [secretary "1.2.3"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [org.clojure/core.memoize "0.5.6"]
                 [com.taoensso/timbre "3.4.0"]
                 [org.omcljs/om "0.8.8"]
                 [prismatic/om-tools "0.3.11"]
                 [cljs-ajax "0.3.4"]
                 [figwheel "0.2.5"]]
  :plugins [[lein-cljsbuild "1.0.5"]
            [lein-figwheel "0.2.5"]]
  :figwheel {:css-dirs ["resources/public/css"]
             :ring-handler {{name}}.routing/routes}
  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/components" "src/cljs" "src/dev"]
                        :compiler {:output-to "resources/public/js/main.js"
                                   :output-dir "resources/public/out"
                                   :optimizations :none
                                   :source-map true}}
                       {:id "components"
                        :source-paths ["src/components"]
                        :compiler {:output-to "resources/public/server/components.js"
                                   :output-dir "resources/public/server/out"
                                   :optimizations :whitespace}}
                       {:id "release"
                        :source-paths ["src/cljs" "src/components"]
                        :compiler {:output-to "resources/public/js/main.js"
                                   :optimizations :advanced
                                   :pretty-print false}}]})
