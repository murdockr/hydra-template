(ns {{name}}.routing
 #+clj (:require [bidi.ring :refer [make-handler]]
                 [{{name}}.render :refer [dev-wrapper render]])
 #+cljs (:require [goog.events :as events]
            [goog.History.EventType :as EventType]
            [bidi.bidi :refer [match-route]]
            [cljs.core.async :refer [<! >! chan put!]]
            [cljs.reader :refer [read-string]]
            [ajax.core :refer [GET]])
 #+cljs (:require-macros [cljs.core.async.macros :refer [go]])
 #+cljs (:import goog.history.Html5History
           goog.Uri))


(def routes
  #+clj (make-handler ["/" {"" (fn [req] {:status 200
                                       :body (dev-wrapper {:view "home"})})}])
  #+cljs ["/" {"" :home}])

#+cljs (defn init-history [app-state]
         (let [history (Html5History.)
               listen (fn [el type]
                        (let [out (chan)]
                          (events/listen el type
                                         (fn [e] (put! out e)))
                          out))
               navigation (listen history EventType/NAVIGATE)]
           (doto history
             (.setUseFragment false)
             (.setPathPrefix "")
             (.setEnabled true)) 
           (go
             (while true
               (let [token (.-token (<! navigation))
                     view (:handler (match-route routes token))]
                 (swap! app-state assoc :view (name view)))))
           (events/listen js/document "click"
                          (fn [e]
                            (let [path (.getPath (.parse Uri (.-href (.-target e))))
                                  title (.-title (.-target e))]
                              (when (not (nil? (match-route routes path)))
                                (. history (setToken path title)))
                              (.preventDefault e))))))
