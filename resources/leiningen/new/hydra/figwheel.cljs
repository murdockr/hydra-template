(ns figwheel
  (:require [figwheel.client :as fw :include-macros true]
            [{{name}}.client :refer [render]]))


(enable-console-print!)

(fw/watch-and-reload
  :jsload-callback
  #(render (. js/document (getElementById "app"))))
